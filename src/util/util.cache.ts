import { Injectable } from '@nestjs/common';
import * as NodeCache from 'node-cache';

@Injectable()
export class ServerCache {
  private cache: NodeCache = new NodeCache({ stdTTL: 0 });

  get(key: string) {
    return this.cache.get(key);
  }
  set(key: string, data: any) {
    return this.cache.set(key, data);
  }
}
