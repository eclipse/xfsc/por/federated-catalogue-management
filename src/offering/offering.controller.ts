import { HttpService } from '@nestjs/axios';
import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import * as jp from 'jsonpath';
import { Public } from 'src/auth/auth.guard';
import { delay, lastValueFrom, map } from 'rxjs';
import { HttpResolver } from 'src/util/util.http-resolver';
import { CreateAttestationDto } from './dto/create-attestation.dto';
import { CreateOfferingDto } from './dto/create-offering.dto';
import { OfferingService } from './offering.service';
import { Interval } from '@nestjs/schedule';
import { Console } from 'console';

@ApiTags('offering/attestation')
@ApiBearerAuth()
@Controller()
export class OfferingController {
  constructor(
    private readonly offeringService: OfferingService,
    private readonly httpresolver: HttpResolver,
    private readonly httpService: HttpService,
  ) { }

  @Post('/offering')
  async addServiceOffering(@Body() body: CreateOfferingDto): Promise<any> {
    var offerings = this.offeringService.getOfferings() ?? {};
    let did = body.did;
    const didsToResolve = [];

    didsToResolve.push(did);

    //console.log.log(process.env.RESOLVER + did);

    while (didsToResolve.length > 0) {
      let didToResolve = didsToResolve.pop();

      if (
        didToResolve.startsWith('urn') || didToResolve.startsWith('did:web:example')
        || didToResolve.startsWith('did:web:sadfds') ||
        didToResolve.startsWith('did:web:did.spherity.dev:did:componentPassport') ||
        !!offerings[didToResolve] ||
        !didToResolve.startsWith('did:web') ||
        didToResolve.startsWith('did:web:registry')
      ) {
        continue;
      }
      console.log(didToResolve)


      offerings[didToResolve] = {};

      const diddocument = await this.httpresolver.Get(
        process.env.RESOLVER + didToResolve,
      );

      for (const element of diddocument['service']) {
        let type = element['type'];

        offerings[didToResolve][type] = {};

        if (type == 'gx-catalog-description') {
          const full = await this.httpresolver.Get(element['serviceEndpoint']);

          var description = {};
          description['full'] = full;
          description['serviceEndpoint'] = element['serviceEndpoint'];

          var json = JSON.stringify(full);
          if (json.includes('trusted-cloud:ServiceOffering'))
            this.offeringService.registerOfferingForUpload(full);
          var searchJson = JSON.parse(json.replace(/@id/g, 'id'));
          var otherDIDs = jp.query(searchJson, '$..id');
          otherDIDs.forEach((element) => {
            didsToResolve.push(element);
          });

          offerings[didToResolve]['attestations'] = {};
          var attestations = jp.query(searchJson, '$..attestationReference');

          console.log(attestations)

          attestations.forEach((att) => {
            offerings[didToResolve]['attestations'][att.name] = att;
          });

          offerings[didToResolve][type] = description;
        }

        if (type == 'gx-trusted-connection') {
          var description = {};
          description['serviceEndpoint'] = element['serviceEndpoint'];
          offerings[didToResolve][type] = description;

          if (!offerings[didToResolve]['connection']) {
            offerings[didToResolve]['connection'] = {};
          }

          if (
            !offerings[didToResolve]['connection'][element['serviceEndpoint']]
          ) {

            var invitiation = ""

            try {
              invitiation = await this.httpresolver.Post(
                element['serviceEndpoint'],
              );
            }
            catch {
              invitiation = await this.httpresolver.Get(
                element['serviceEndpoint'],
              );
            }

            console.log(invitiation)

            let payload = {
              invitationUrl: invitiation['data']['invitationUrl'],
              autoAcceptConnection: true,
            };

            console.log(payload);
            const connection = await this.httpresolver.Post(
              process.env.CM_OCM,
              payload,
            );




            offerings[didToResolve]['connection']['description'] = connection;
            offerings[didToResolve]['connection']['id'] = connection['data']['id'];
            console.log(connection);
          }
        }
      }
    }
    this.offeringService.setOfferings(offerings);
    return offerings;
  }

  @Post('/attestation')
  async getAttestation(@Body() createAttestationDto: CreateAttestationDto) {
    var offerings = this.offeringService.getOfferings();

    let did = createAttestationDto.did;
    let attestation = createAttestationDto.name;
    console.log(did)
    console.log(attestation)

    if (!offerings[did]) throw new BadRequestException('DID not found');

    if (!offerings[did]['attestations'])
      throw new BadRequestException('No attestations available');

    if (!offerings[did]['attestations'][attestation])
      throw new BadRequestException('No such attestation available');

    if (!offerings[did]['attestations'][attestation]['proof']) {
      let connectionId = offerings[did]['connection']['id'];
      let definition =
        offerings[did]['attestations'][attestation]['presentationDefinition'];
      let constraints =
        definition['input_descriptors'][0]['constraints']['fields'];
      let attributes = [];
      console.log(constraints)
      constraints[1]['path'].forEach((element) => {
        let attribute = {};
        attribute['attributeName'] = element.substring(2);
        attribute['schemaId'] = constraints[0]['filter']['const'];
        //attribute['credentialDefId'] = constraints[0]['filter']['const'];
        attributes.push(attribute);
      });

      console.log(attributes);

      let proofRequest = {
        comment: 'Please proof...',
        connectionId: connectionId,
        attributes: attributes,
      };
      //console.log.log(JSON.stringify(proofRequest));
      const result = await this.httpresolver.Post(
        process.env.PROOF_OCM + '/send-presentation-request',
        proofRequest,
      );
      console.log(result)
      let qid = did + attestation
      if (
        Object.keys(this.offeringService.getOpenAttestations as any).includes(
          qid,
        ) == undefined
      )
        console.log(qid)
      this.offeringService.registerAttesttionForQueue({
        "did": did,
        "pid": result["data"]["presentationId"],
        "attestation": attestation
      });
      return { status: 202, message: 'accepted' };
    } else {
      return offerings[did]['attestations'][attestation]['proof'];
    }
  }

  @Interval(2000)
  async updateAttestation() {
    var attestations: any = this.offeringService.getOpenAttestations() ?? [];
    console.log(attestations)
    for (var i = 0; i < attestations.length; i++) {
      console.log(attestations[i])
      var offerings = this.offeringService.getOfferings() ?? {};
      const result = await lastValueFrom(
        this.httpService
          .get(
            process.env.PROOF_OCM +
            '/find-by-presentation-id?presentationId=' +
            attestations[i]["pid"],
          )
          .pipe(map((resp) => resp.data)),
      );

      if (result.status == '404') {
        attestations = attestations.splice(i, 1);
      } else if (result.statusCode == '200' && result.data.state == 'done') {
        offerings[attestations[i]["did"]]['attestations'][
          attestations[i]["attestation"]
        ]['proof'] = result.data.presentations[0].credentialSubject;
        console.log(result.data.presentations[0].credentialSubject)
        attestations = attestations.splice(i, 1);
        this.offeringService.setOfferings(offerings);
      }
    }
    this.offeringService.setOpenAttestations(attestations);
  }

  startComplianceCheck(): string {
    return null;
  }
}
