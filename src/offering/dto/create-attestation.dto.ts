import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateAttestationDto {
  @ApiProperty()
  @IsString()
  did: string;
  @ApiProperty()
  @IsString()
  name: string;
}
