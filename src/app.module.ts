import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CatalogueModule } from './catalogue/catalogue.module';
import { OfferingModule } from './offering/offering.module';
import { UtilModule } from './util/util.module';

@Module({
  imports: [
    UtilModule,
    ConfigModule.forRoot(),
    CatalogueModule,
    AuthModule,
    OfferingModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
